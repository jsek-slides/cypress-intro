# Cypress.io

<hr>

E2E & UI Testing for developers

---

<br>

Once upon a time...

<br>

I worked for Tango Team

<br>

battling visual consistency <!-- of "Add to" dialogs -->

<br>

and my sword was called [Selenium IDE](https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd)

<br>

---

![](images/google/zelda.jpg)

---

Why do we even need E2E tests?

---

> ![](images/google/humor.jpg)

---

<i>Testing shows the presence, not the absence of bugs.</i>

<cite>E.W. Dijkstra</cite>

---

> ![](images/google/dev-cycle.jpg)

---

## What is Cypress?

> <g>New testing tool written entirely in</g> `JavaScript` <g>that</g> _**helps**_ <g>developers write</g> automated tests <g>for</g> web applications.

<!-- Cypress creator plan to add the support for other browsers anyway -->
<!-- https://github.com/cypress-io/cypress/issues/310 -->

---

### Not new concept

TestCafe has been rewritten in similar way <small>(dropped Selenium as base)</small>

<hr>

### Alternatives

- [Zalenium](https://opensource.zalando.com/zalenium/)<small>*</small>
- [TestCafe](https://devexpress.github.io/testcafe/) + [Studio](https://testcafe-studio.devexpress.com)
- [TheIntern](https://github.com/theintern/intern-examples/blob/master/angular-example/tests/functional/Todo.ts)
- [CodeceptJS](https://codecept.io/)
- [Zombie.js](http://zombie.js.org/)
- [Nemo.js](http://nemo.js.org/)

---

### Cypress focus on developers

<br>

We wish that...

- *__🗸__* changes wouldn't break existing features
- *__🗸__* refactoring could feel safe

<br>

---

### Business goal

```grid(1:1,#151515)
Productivity + Quality
```

<small>happy developers create better software</small>

---

> ### Developers often complain about automated tests

<br>

> <iframe width="800" height="450" src="https://www.youtube-nocookie.com/embed/pJ349YntoIs?rel=0&amp;start=260" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

---

### Questions to ask yourself

- _How long does it take to..._
  - setup tests against localhost
  - add new test
  - troubleshoot and fix a failing test
  - analyze report full of flaky tests results


- _How much you are willing to invest in..._
  - error handling tests
  - hacks that can force specific state


- _How much time before..._
  - we let newhire write new tests
  - we have 100% green without muting

---

![](images/google/no-thanks-were-too-busy.jpg)

---

### How does Cypress solve the problem?

[Features](https://www.cypress.io/features/)

```grid(3:2,#33404D:#00523a,gap:.3em)
Easy to start
Fast & intuitive
Easy debugging
Docs quality
--teamcity
Resilience
```

<!--
    Speed - finally possible to TDD, take shortcuts
    Ease of use - intuitive UI, commands: open/run, just works,  kitchen sink
    Debugging - we need this because assertions may fail due to:
    - some network request failed
    - something broke few steps back
    - timeout exception?... sooo unhelpful
    Resilience - sometimes tests are flaky because Selenium node (VM) is outside test runner
-->
<!-- Severity: cypress-serenity-reporter - npm -->

---

![](images/google/learning-curves.jpg)

---

### Quick start

<br>

```sh
> npm init -y
> npm install cypress
> node_modules\.bin\cypress open
```

<br>

---

![](images/screenshots/features.png)

---

### Comparison: starting with Protractor

1. Start Selenium Hub and node
  - <small>`docker run ... selenium/standalone-chrome`</small>
1. `browser.waitForAngularEnabled(false)`
1. Create `protractor.conf.js`
1. Figure out how to run on file change
  - <small>if you'd like to try it for TDD</small>

or

<small>...assuming you installed Chrome in ~\AppData</small>

1. Install ChromeDriver
1. ...
1. Configure with `--headless --disable-gpu`
1. ...

---

<br>

> <i class="fa fa-info-circle"></i> Protractor seems to be very fast with Docker, so is worth trying anyway.

<br>

---

#### package.json

```json
{
    ...
    scripts: {
        "open": "cypress open --detach",
        "start": "cypress run",
        "ci:test": "cypress run --teamcity"
    }
    ...
}
```

---

#### Typical development flow

1. Outline new tests
1. `xdescribe()`, `xit()`
   <br><small>supressing other tests in the file</small>
1. Visit target website
1. Choose selectors
1. `.click()`, `.type()`, `.click()`
1. Write assertions

---

```js
describe('[Newsletter form]', () => {

    describe('open/close', () => {
        it('should be possible to open newsletter popup', () => {})
        it('should be possible to close newsletter popup', () => {})
    })

    describe('validation', () => {
        it('should not allow empty email', () => {})
        it('should not allow invalid email', () => {})
        it('should accept and send valid email', () => {})
        it('should be disabled while loading', () => {})
    })

    describe('error handling', () => {
        it('should display feedback if EmailOctopus rejected submission', () => {})
    })
})
```

---

#### Protip #1

See your test __*red*__

---

```js
describe('[Newsletter form]', () => {

    beforeEach(() => {
        cy.visit('/blog.html');
        cy.get('.newsletter-show').click()
    })

    describe('open/close', () => {
        it('should be possible to open newsletter popup', () => {
            cy.get('.newsletter-signup').should('be.visible')
        })

        it('should be possible to close newsletter popup', () => {
            cy.get('.modal-close'      ).click()
            cy.get('.newsletter-signup').should('not.be.visible')
        })
    })
})
```

---

#### Protip #2

Less `it(...)`'s

<small>because performance matters</small>

---

#### Protip #2,5

*__<i class="fas fa-glasses"></i>__* Readability matters more

---

```js
describe('[Newsletter form]', () => {

    beforeEach(() => {
        cy.visit('/blog.html');
    })

    describe('open/close', () => {
        it('should be possible to open and close newsletter popup', () => {
            cy.get('.newsletter-show'  ).click()
            cy.get('.newsletter-signup').should('be.visible')
            cy.get('.modal-close'      ).click()
            cy.get('.newsletter-signup').should('not.be.visible')
        })
    })
})
```

---

#### Protip #3

Add `tsconfig.json` even if you don't write TypeScript

<small>...assuming you use VSCode</small>

---

### Stubbing server

With request stubbing you can...

- Let the runner know it should wait for some request
- Save response to file<small>*</small>
- Serve fixtures from files (`*.json`)
- Stub failed (`5xx`, `4xx`) response to test error handling
- Force response delay to test _loading_ states

What you gain:

- More resilient tests
  - <small>database can be offline or empty</small>
- Test non-standard cases (that require hacks)
  - <small>empty / loading / timeout / error</small>

---

```js
cy.server()
  .route(
    '**/Attributes?*',
    'fixture:attributes.json'
  )
  .as('attributes')

  .route(
    'POST',
    '**/Query',
    'fixture:query.json'
  )
  .as('query')
```

---

#### There are even more ways to speed up tests with Cypress

<small>Read about them in [docs](https://docs.cypress.io/guides/references/best-practices.html)</small>

<br>

Best Practice:
- test specs in isolation
- programmatically log into your app
- take control of your app’s state

---

```js
it('should replace initial name with valid one', () => {
    cy.server()
        .route({
            method: 'POST',
            url: '**/CheckName',
            response: {
                CheckedName: 'Untitled query',
                AllowedName: 'VALID NAME'
            }
        }).as('InitialCheck')

    cy.get('[qa=save-query] button')
        .click();

    cy.get('[qa=saved-query-name]')
        .should('have.value', 'VALID NAME')
});
```

---

```js
describe('error handling', () => {
    it('should display feedback if EmailOctopus rejected submission', () => {
        cy.server()
            .route({
                method: 'POST',
                url: 'https://emailoctopus.com/**',
                response: { error: "Stubbed failure" },
                status: 400
            }).as('failure')

        cy.get('input[type=email]')
            .type('valid_email@example.com')

        cy.get('button[type=submit]')
            .click()

        cy.get('.signup-error-message')
            .should('be.visible')
    })
})
```

---

#### Protip #4

Go one step further
with *__Docker__*

---

```yml
version: '3'

services:

  website:
    build:
      context: .
      dockerfile: docker/.app.Dockerfile

  cypress:
    build:
      context: .
      dockerfile: docker/.tests.Dockerfile
    depends_on:
      - website
    environment:
      - CYPRESS_BASE_URL=http://website
    volumes:
      - ./mochawesome-report:/app/mochawesome-report
```

---

#### Protip #4,5

Use *__yarn__* when restoring `node_modules`
while building container image

<br>
<small>Waiting for "npm install" is frustrating</small>

---

```dockerfile
FROM cypress/base:10

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn

COPY cypress.json .
COPY tsconfig.json .
COPY webpack.config.js .
COPY cypress ./cypress

ENTRYPOINT [ "yarn", "test" ]
```

---

```sh
> docker-compose build
> docker-compose up \
  --abort-on-container-exit
```

---

### Integration with CI/CD

1. Prefer *__Docker__*
1. `--teamcity`
1. `CYPRESS_BASE_URL`
1. Set any HTML reporter
  <br><small>(e.g. `mochawesome`)</small>

---

```impact
One step further
.........................................................................................................................................................
Dashboard
```

---

[![](images/screenshots/dashboard.png)](https://dashboard.cypress.io/#/projects/9dby2t/runs)

---

1. Login with GitHub account
1. Generate project ID
1. Copy Record Token <small>into CI agent parameters</small>

```sh
> cypress run --record --key $CYPRESS_TOKEN
```

---

### Dashboard capabilities

- single store of reports
  <br><small>with screenshots and videos</small>

_Worth money?_

<br>

Watch for new features to come:
- [Detailed logs view](https://github.com/cypress-io/cypress/issues/448)
- Parallelization and load balancing
- See [roadmap](https://docs.cypress.io/guides/references/roadmap.html#Dashboard-Service)

<!-- Problem: Pending when test runner actually failed (e.g. on writing report to disk) -->

---

Testing is a **_controversial_** topic

---

![](images/google/disagree.jpg)

---

![](images/google/pyramid.png)

---

# ROI

<small>~ cost effective</small>

write tests that **bring value** and _keep proportions_ so we invest least amount of _**$$$**_

---

Who creates ~~bugs~~ regression?

Who fixes ~~bugs~~ regression?

<br>

Who should be notified **first**?

**How long** should developers wait?
<br><small>Until next release?</small>

<br>

Sorry, __*I feel*__ like our selenium-based tests are useless :(
<br><small>Is it just me?</small>

Sorry again, __*I feel*__ like we waste energy on running unit tests in CI
<br><small>Based on how the codebase grow</small>

---

<small>Recommended&nbsp;reading:</small>

[Unit Test Fetish](http://250bpm.com/blog:40)

---

<!-- ```
Test Maintenance scenario
- a test is consistently red
- you try to understand it... read error "IndexOutOfRange/EmptyCollection" -> target element missing
- what happened? did the page loaded correctly? did something broke few steps back? is it just obsolete selector?
- read code -> manual clicks -> read code -> ...
``` -->

## What this Cypress brings to the table?

 - significantly easier to maintain tests
 - easier to test UI in isolation
 - trivial to test error handling
 - trivial TDD with acceptance tests
 - higher ROI

---

### ...and these tests

 - have better possibility to catch regression
   <br><small>e2e vs integration/api/unit</small>
 - are more resilient
   <br><small>run inside browser vs vm🠚hub🠚node🠚browser</small>
 - are more readable
   <br><small>vs nightwatch.js / protractor</small>
 - are easier to adopt
   <br><small>because of GUI / docs / setup / examples</small>

---

### ...and...

 - are cheaper
   <br><small>less time/$$$ to get results</small>
 - use technology backed by a company
   <br><small>instead of Selenium backed by ...?</small>
 - could run on commit
   <br><small>with Docker</small>
 - generate screenshots and videos
   <br><small>out of the box</small>
 - enterprisey dashboard

---

### Cypress problems

- ~~Only Chrome~~
  <br><small>manually check for other browsers on Stage</small>
- Chrome bugs
- TypeScript compilation errors require GUI restart
- Sometimes needs <small>`cy.wait(100)`</small> or <small>`.click({force:true})`</small>
- Marketing voice
  <br><small>neglects frameworks / workarounds for Selenium</small>
- ~~Code gets a bit messy~~
  <br><small>I am a bit too lazy to refactor it :P</small>
- Crashed while running against UAT <!-- 18 x Impersonate & redirects -->

<!-- For other browsers we can manually click-through once Stage is ready
- not a problem if they don't get "automation" and we fix these few bugs last

// e.g. date rendering in Edge (even IE 11 worked) <- is it something you would write tests for? -->

---

> `Final point`

---

<br>

_<i>I can't write unit tests that would give me more confidence</i>_

<br>
I write simple & readable code instead

<hr>

_<i>I don't have time to prepare service mocks</i>_

<br>
So I don't have any integration tests

<br>

---

### Cypress lets me gain more confidence

<i class="fa fa-check-circle"></i> Pleasable experience

<i class="fa fa-check-circle"></i> Tests are worth running

<i class="fa fa-check-circle"></i> Low maintenance costs
<br><small>1~2 hours per User Story<br><small style="vertical-align: 1em">(not more than unit tests)</small></small>

---

### Resources

<br>

- [Intro](https://www.cypress.io/how-it-works/) | [Docs](https://docs.cypress.io/guides/core-concepts/test-runner.html) | [FAQ](https://docs.cypress.io/faq/questions/using-cypress-faq.html) | [Blog](https://www.cypress.io/blog/)
- <i class="fas fa-graduation-cap"></i> [End to End testing with Cypress](https://egghead.io/courses/end-to-end-testing-with-cypress)
- <i class="fab fa-youtube"></i> [AssertJS 2018 | Testing Best Practices](https://www.youtube.com/watch?v=5XQOK0v_YRE)
- <i class="fab fa-youtube"></i> [Intro for Angular developers](https://www.youtube.com/watch?v=7N63cMKosIE)
- <i class="fab fa-youtube"></i> [Testing The Way It Should Be](https://www.youtube.com/watch?v=pJ349YntoIs)
- <i class="fab fa-youtube"></i> [Cypress channel](https://www.youtube.com/channel/UC-EOsTo2l2x39e4JmSaWNRQ/videos)
- <i class="fab fa-medium"></i> [Cypress.io & Docker: the Ultimate E2E Stack](https://hackernoon.com/cypress-io-docker-the-ultimate-e2e-stack-a20ee25654b1)

<br>

<!--
https://testing.googleblog.com/2015/04/just-say-no-to-more-end-to-end-tests.html
https://sphereinc.com/achieve-quality-code-and-roi-through-test-automation/
-->

---

##### <i class="fas fa-cookie-bite"></i> ... Thanks

<!--

some ideas:
 - Avoiding learning even more with custom generators
   - https://www.hygen.io/create
 - Docker build optimizations
   - https://codefresh.io/wp-content/uploads/2018/07/docker-layers.png

 -->